# QA Avenue Code 

Test Automation for QA Engineer position

## How to run the tests

**1** - Install Node.js: https://nodejs.org/en/download/

**2** - Make a repository clone.

**3** - Open the CMD and navigate until be on the following directory 'qa-avenuecode' and type the following command to install the dependencys:
```
  npm install
```

## **4 -** Running the tests

Run `npm t` to run the e2e tests on both Chrome and Firefox headless browsers (Chrome tests will run first, then Firefox tests will run).

Run `npm run test:chrome` to run the e2e tests only on Chrome headless browser (all tests will run in only one browser instance).

Run `npm run test:firefox` to run the e2e tests only on Firefox headless browser (all tests will run in only one browser instance).

Run `npm run test:multi` to run the e2e tests on both Chrome and Firefox headless browsers in parallel.

Run `npm run test:chrome:parallel` to run the e2e tests only on Chrome headless browser (each spec file will be executed in a different browser instance in parallel).
