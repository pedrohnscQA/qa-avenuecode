const Helper = require("protractor-helper")

const LoginPage = require('../page-objects/login.po')
const MyTask = require('../page-objects/myTasks.po')

describe("Scenario: As a ToDo App user \n I should be able to create a task \n So I can manage my tasks", () => {
    //#region Test Parameters
    const taskTitle = 'Test'
    const oneCharacter = 'p'
    const twoHundredAndFiftyCharacters = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'

    const loginPage = new LoginPage();
    const myTasks = new MyTask();

    beforeEach(() => {
        loginPage.visit();
        loginPage.doLogin("pedrohnsc@gmail.com", "12345678");
        myTasks.acessTasks();
    });

    afterEach(() => {
        loginPage.doLogout();
    });

    it("GIVEN I'm logged \n -  WHEN I acess the task page \n -  AND add a task clicking on the plus button \n -  THEN I find that a new task has been created", () => {
        myTasks.addTask(taskTitle);
        expect(myTasks.getTaskName()).toContain(taskTitle);
    });

    it("GIVEN I'm logged \n -  WHEN I acess the task page \n -  AND add a task hitting enter \n -  THEN I find that a new task has been created", () => {
        myTasks.addTaskWithEnterButton(taskTitle)
        expect(myTasks.getTaskName()).toContain(taskTitle);
    });

    it("GIVEN I'm logged \n -  WHEN I acess the main page \n -  THEN I see 'Hey NAME, this is your todo list for today:'", () => {
        expect(myTasks.onTopMessageWithName.getText()).toContain('Hey Pedro Henrique, this is your todo list for today:');
    });

    it("GIVEN I'm logged \n -  WHEN I acess the main page \n -  THEN I should always see the My tasks on the navBar'", () => {
        Helper.waitForElementVisibility(myTasks.myTaskNavBar);
    });

    it("GIVEN I'm logged \n -  WHEN I click on My tasks on the navBar \n -  THEN I should be redirected to My tasks page", () => {
        Helper.waitForElementVisibility(myTasks.toDoList);
    });

    it("GIVEN I'm logged \n -  WHEN I acess the task page \n -  AND try to add a task with one character \n -  THEN I should see that it's not possible to register with less than 3 characters", () => {
        myTasks.addTask(oneCharacter);
        Helper.waitForTextNotToBePresentInElement(myTasks.taskName, oneCharacter);
    });

    it("GIVEN I'm logged \n -  WHEN I acess the the task page \n -  AND try to add a task with 251 characters \n -  THEN I should see that it's not possible to register with more than 250 characters", () => {
        myTasks.addTask(twoHundredAndFiftyCharacters);
        Helper.waitForTextNotToBePresentInElement(myTasks.taskName, twoHundredAndFiftyCharacters);
    });
});