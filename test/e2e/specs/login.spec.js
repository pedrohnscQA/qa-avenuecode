const LoginPage = require('../page-objects/login.po');
const Messages = require('../constants/messages')

describe('Given im in the login page', () => {

    const loginPage = new LoginPage()
    const messages = new Messages()

    beforeEach(() => {loginPage.visit()})
    
    it('As a register user, i should log in with Success', () => {
        loginPage.doLogin("pedrohnsc@gmail.com", "12345678");
        expect(messages.getSignedInSucessfully()).toBe('Signed in successfully.')
    });

    afterEach(()=>{loginPage.doLogout()})
});

describe('Given im in the login page and try to enter with invalid credentials', () => {

    const loginPage = new LoginPage()
    const messages = new Messages()

    beforeEach(() => {loginPage.visit()})
    
    it('Try to login with wrong password', () => {
        loginPage.doLogin("pedrohnsc@gmail.com", "789");
        expect(messages.getinvalidEmailPasswordMessage()).toBe('Invalid email or password.')
    });

    it('Try to login with wrong email', () => {
        loginPage.doLogin("pedrohnschueheu@gmail.com", "12345678");
        expect(messages.getinvalidEmailPasswordMessage()).toBe('Invalid email or password.')
    });

    it('Leave all fields in blank space', () => {
        loginPage.doLogin("", "");
        expect(messages.getinvalidEmailPasswordMessage()).toBe('Invalid email or password.')
    });
});