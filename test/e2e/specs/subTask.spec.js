const Helper = require("protractor-helper")

const LoginPage = require('../page-objects/login.po')
const MyTask = require('../page-objects/myTasks.po')
const Subtask = require('../page-objects/subTask.po')

describe("Scenario: As a ToDo App user \n I should be able to create a subtask \n So I can break down my tasks in smaller pieces", () => {
    //#region Test Parameters
    const taskName = 'SubTask Test'
    const subTaskName = 'avenueCode test';

    const loginPage = new LoginPage()
    const myTasks = new MyTask()
    const subTask = new Subtask()

    beforeEach(() => {
        loginPage.visit();
        loginPage.doLogin("pedrohnsc@gmail.com", "12345678");
        myTasks.acessTasks();
        myTasks.addTask(taskName);
    });

    afterEach(() => {
        loginPage.doLogout();
    });

    it("GIVEN I'm logged \n -  WHEN I acess the task page \n - AND add a Sub Task \n  - Then i can break my task into smaller pieces", () => {
        expect(subTask.btnManageSubTask.getText()).toContain('Manage Subtasks');
    });

    it("GIVEN I'm logged \n -  WHEN I acess the task page \n - AND add a Sub Task \n  - Then i can break my task into smaller pieces", () => {
        subTask.addSubTask(subTaskName);
        Helper.waitForElementVisibility(subTask.manageSubtasksButton('1'));
    });

    it("GIVEN I'm logged \n -  WHEN I acess the task page \n - AND try to add a Sub Task with the required fields in blank \n  - THEN I can't register the subtask", () => {
        subTask.addSubTask('', '');
        Helper.waitForElementVisibility(subTask.manageSubtasksButton('0'));
    });

    it("GIVEN I'm logged \n -  WHEN I acess the task page \n - AND try to add a Sub Task with the required due date in blank \n  - THEN I can't register the subtask", () => {
        subTask.addSubTask(subTaskName, '');
        Helper.waitForElementVisibility(subTask.manageSubtasksButton('0'));
    });

    it("GIVEN I'm logged \n -  WHEN I acess the task page \n - AND try to add a Sub Task with the required subTask description in blank \n  - THEN I can't register the subtask", () => {
        subTask.addSubTask('');
        Helper.waitForElementVisibility(subTask.manageSubtasksButton('0'));
    });
})