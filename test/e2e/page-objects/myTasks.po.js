const Helper = require("protractor-helper");

class MyTasks {

    constructor() {
        this.myTaskNavBar = element(by.id('my_task'))
        this.task = element(by.id('new_task'));
        this.btnAddTask = element(by.css('[ng-click="addTask()"]'));
        this.taskName = element.all(by.css('td.task_body.col-md-7 > a')).get(0);
    };

    acessTasks() {
        Helper.click(this.myTaskNavBar);
    };

    addTask(taskTitle) {
        Helper.fillFieldWithText(this.task, taskTitle);
        Helper.click(this.btnAddTask);
    };

    addTaskWithEnterButton(taskTitle) {
        Helper.fillFieldWithTextAndPressEnter(this.task, taskTitle)
    }

    getTaskName() {
        return this.taskName.getText();
    };
};
module.exports = MyTasks;