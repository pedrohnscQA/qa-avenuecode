const Helper = require("protractor-helper");

class Login {
    
    constructor() {
        this.inputEmail = element(by.id('user_email'));
        this.inputPassword = element(by.id('user_password'));
        this.buttonSubmit = element(by.name('commit'));
        this.logout = element(by.linkText('Sign out'));
    }
    
    visit() {
        browser.get('users/sign_in');
    };

    doLogin(email, password) {
        Helper.fillFieldWithText(this.inputEmail, email);
        Helper.fillFieldWithText(this.inputPassword, password);
        Helper.click(this.buttonSubmit);
    };
    
    doLogout() {
        Helper.click(this.logout);
    };

};
module.exports = Login;