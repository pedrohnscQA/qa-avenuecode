const Helper = require('protractor-helper')
const moment = require("moment");

class Subtask {
    constructor() {
        this.btnManageSubTask = element.all(by.css('[ng-click="editModal(task)"]')).get(0);
        this.subTaskDescription = element(by.id('new_sub_task'));
        this.dueDate = element(by.id('dueDate'));
        this.btnAddSubTask = element(by.id('add-subtask'));
        this.btnCloseSubTask = element(by.cssContainingText('button', 'Close'));
        this.toDoList = element(by.css('div.container > h1'));
        this.inputEditableSubtaskDescription = element.all(by.css('input[ng-model="$data"]')).get(0);
        this.onTopMessageWithName = element(by.css('body > div.container > h1'));
    }

    getSubTaskName() {
        return this.subTaskName.getText();
    };

    addSubTask(subTaskDescription, today = moment().format('DD/MM/YYYY')) {
        Helper.click(this.btnManageSubTask);
        Helper.fillFieldWithText(this.subTaskDescription, subTaskDescription);
        Helper.clearFieldAndFillItWithText(this.dueDate, today);
        Helper.click(this.btnAddSubTask);
        Helper.click(this.btnCloseSubTask);
    };

    manageSubtasksButton(numberOfSubTasks) {
        return element(by.cssContainingText('button', '(' + numberOfSubTasks + ') Manage Subtasks'));
    };

    subtaskTitleElement(position = 0) {
        return element.all(by.css('td.task_body.col-md-8 > a')).get(position);
    };

    returnSubtaskTitle(position) {
        position--;
        const subtaskTitle = this.subtaskTitleElement(position);
        return subtaskTitle.getText();
    };
}
module.exports = Subtask;