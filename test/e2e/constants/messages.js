const Helper = require('protractor-helper')

class Messages {
    constructor() {
        this.invalidEmailPasswordMessage = element(by.cssContainingText('.alert.alert-warning', 'Invalid email or password.'));
        this.signedInSucessfully = element(by.cssContainingText('.alert.alert-info', 'Signed in successfully.'));
    }

    getinvalidEmailPasswordMessage() {
        Helper.waitForElementVisibility(this.invalidEmailPasswordMessage)
        return this.invalidEmailPasswordMessage.getText()
    }

    getSignedInSucessfully() {
        Helper.waitForElementVisibility(this.signedInSucessfully)
        return this.signedInSucessfully.getText()
    }
}

module.exports = Messages;