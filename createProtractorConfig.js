module.exports = providedConfig => {
    const defaultConfig = {
        specs: ["../specs/*.spec.js"],
        baseUrl: "https://qa-test.avenuecode.com/",
        directConnect:true,
        onPrepare: () => {
            browser.waitForAngularEnabled(false);
        }
    }
    return Object.assign({}, defaultConfig, providedConfig);
};